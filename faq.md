# FAQ
## Why did you call multiple apis from the spa directly instead of using api gateway ?
LLM Tradeoff Analysis:
```
Calling Multiple APIs from a Frontend SPA:

    Direct Integration: In this scenario, a Single Page Application (SPA) directly calls multiple external APIs. This approach is straightforward and might be quicker to implement initially.
    Pros:
        Simplicity: Easier to implement without the need for an intermediate service.
        Direct Control: The frontend has direct control over the APIs it consumes, which can be beneficial for handling specific API responses or errors.
    Cons:
        Cross-Origin Resource Sharing (CORS) Issues: Direct calls from the browser to different domains can lead to CORS issues, requiring additional configuration on the server-side.
        Security Concerns: Managing API keys and sensitive information on the client-side can pose security risks.
        Scalability and Maintenance: As the number of APIs increases, managing and maintaining direct API calls can become complex and unwieldy.
        Performance: Multiple API calls from the frontend can increase load times and affect user experience.

Creating an API Gateway:

    Indirect Integration through a Unified Interface: An API Gateway acts as an intermediary between the frontend SPA and multiple backend services or external APIs. It aggregates the APIs into a single interface.
    Pros:
        Centralized Management: Simplifies API management by providing a single point of entry for all external services.
        Security: Offers a layer to implement authentication, authorization, and secure handling of API keys without exposing them to the client-side.
        Cross-cutting Concerns: Enables centralized handling of cross-cutting concerns like logging, rate limiting, and CORS.
        Performance Optimization: Potential to implement caching and reduce the number of requests made from the client by aggregating data.
    Cons:
        Complexity: Introduces an additional layer of complexity and may require more time to set up and maintain.
        Single Point of Failure: If not properly designed, the API Gateway can become a bottleneck or single point of failure.

```

From this its clear that a gateway is wrong for our use case.  


## Why aren't you using a database for dnp3/modbus systems, instead of flat files?
LLM Tradeoff Analysis:
```
Large Configuration File:

    Pros:
        Simplicity: Easier to deploy and manage without the overhead of database connections.
        Performance: Reading from a file at startup can be faster and doesn't incur the latency of database queries for static data.
        Version Control: Easier to track changes and manage versions of the configuration.
    Cons:
        Scalability: As the configuration grows, it can become cumbersome to manage and read.
        Flexibility: Less flexible in terms of querying capabilities compared to a database.

Database:

    Pros:
        Scalability: Better suited to handle large amounts of data and complex queries.
        Management: Offers tools for data management, backup, and recovery.
        Dynamic Updates: While your case mentions static data, if requirements change, a database can easily accommodate dynamic updates without application restarts.
    Cons:
        Overhead: Introduces additional complexity and overhead in terms of setup, maintenance, and performance.
        Boot Time: Can increase the boot time of the application if data needs to be loaded from the database.

Conclusion:
For static data that never changes and is only read at boot time, a large configuration file is generally acceptable and might even be preferable due to its simplicity and performance benefits. However, it's crucial to ensure the file remains manageable and well-structured. If the configuration data grows in complexity or starts changing, transitioning to a database might become a more scalable and flexible solution.
```


## Why don't you don't you forgo mqtt and consume dnp3/modbus and use websockets for the frontend?

MQTT is the industry standard. Plays well with other IoT products. What web teams in this industry are use to consuming.  Its a standard that can be published with some like asyncapi.com. Can be sold as an api product on its own.



## Isn't EKS overkill?
Yes.  People wanna learn cloud stuff. Just learn it once. But yes. Way unnecessary. A VPS provider and docker compose on an on prem server would be fine. I hope one day  to remove this when there's less demand and the industry realizes how foolish it is for most loads. 


