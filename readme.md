![](https://fsiot-branding-assets.s3.amazonaws.com/banner.svg)

# Overview

This series of courses is designed to teach real world skills when implementing industrial and consumer IoT systems. It
covers everything from the sensors, to the UI (mobile and web), to predictive machine learning models. The three languages used are

- 🌊 Typescript: Realtime UIs and APIs
- 🐍 Python: Machine Learning and Platform Engineering
- 🦀 Rust: SCADA Protocols and Embedded Systems

This course will help you create the an aproproatie ai agent  with the correct context to assist you in the different areas of the project.  See the deployment diagrams below (the broker is omitted for simplicity).

The project will simulate a BESS system. Also collect the forecast the solar production (supply) from ERCOT, to help the user make decisions on when to charge and discharge the battery. 

## Deployment Diagram

```plantuml

rectangle mock_modbus_server
rectangle mock_dnp3_outstation
node ac_meter_module
node dc_meter_module
cloud ercot_api
rectangle cluster #line.dashed {
    rectangle scada_mqtt_gateway
    rectangle domain_api
    database timeseries
    collections forecast_api 
    database document
    rectangle realtime_dashboard
    rectangle power_controller
    rectangle realtime_dashboard
}

ac_meter_module -d- power_controller
dc_meter_module -d- power_controller
mock_dnp3_outstation -d- scada_mqtt_gateway
mock_modbus_server -d- scada_mqtt_gateway
ercot_api -r- timeseries
power_controller -d- domain_api
scada_mqtt_gateway -d- domain_api
domain_api -d- realtime_dashboard
domain_api -r- document
timeseries -r- forecast_api
forecast_api -d- realtime_dashboard
```

By the end of the courses you should be able to:

- Build SCADA system that translate between protocols DNP3, Modbus, and MQTT
- Build Embedded Rust Systems that publish sensor readings and subscribe to commands for actuators
- Build an ML timeseries API  with automatic drift detection and retraining
- Build a documented realtime API using MQTT
- Work with time series databases
- Build onprem and aws based systems
- Unit, integration and end-to-end test the whole system.

## What this course is not

- a tutorial on scada
- a tutorial on machine learning
- a tutorial on react or react native
- a tutorial on mqtt
- a tutorial on REST APIs
- a tutorial on authentication and authorization

This course assumes you've consumed resources on these topics before. If you haven't, I recommend the following
resources:

- [modbus](https://udemy.com/course/mastering-modbus-rs485-network-communication)
- [dnp3](https://udemy.com/course/an-introduction-to-the-dnp3-scada-communications-protocol)
- [mqtt v5](https://www.hivemq.com/mqtt/mqtt-5/)
- [designing ml systems](https://a.co/d/hFJKpsk)
- [time series analysis](https://www.packtpub.com/product/time-series-analysis-with-python-cookbook-2e-second-edition/9781805124283)
- [embedded rust](https://docs.rust-embedded.org/book/)
- [rust on esp](https://docs.esp-rs.org/book/)


## Sequence Diagrams
```plantuml
database document
participant domain_api
participant gateway
participant controller
participant broker
database timeseries
participant forecast_api
participant ercot_api   
participant dashboard
document -> domain_api: generate topics
domain_api -> gateway: GET /topics
domain_api -> controller: GET /topics
domain_api -> dashboard: GET /topics
gateway -> broker: pub dnp3 & modbus
controller -> broker: pub direct sensor data
broker -> timeseries: writes to db
broker -> dashboard: renders live data
timeseries -> forecast_api: trains model
ercot_api -> forecast_api: GET /solar-production
forecast_api -> dashboard: renders prediction

```


# Project Structure

Throughout this course we will build a generic real time dashboard for web and mobile. It will have an asyncapi and openapi docs for people to build their own clients.  With the data collected in the time series
db, we can train a model to forecast future values.

## Repositories

The following repositories are used in this course
- 🦀 [`mock-modbus-server`](https://gitlab.com/fullstack-iot/mock-modbus-server)
- 🦀 [`mock-dnp3-outstation`](https://gitlab.com/fullstack-iot/mock-dnp3-outstation)
- 🦀 [`power-controller`](https://gitlab.com/fullstack-iot/power-controller)
- 🦀 [`scada-mqtt-gateway`](https://gitlab.com/fullstack-iot/scada-mqtt-gateway)
- 🌊 [`domain-api`](https://gitlab.com/fullstack-iot/domain-api)
- 🌊 [`realtime-dashboard`](https://gitlab.com/fullstack-iot/realtime-dashboard) (monorepo)
- 🐍 [`forecast-api`](https://gitlab.com/fullstack-iot/forecast-api) (monorepo)
- 🐍 [`deployment-tooling`](https://gitlab.com/fullstack-iot/deployment-tooling)

# Course Outline

This course takes a holistic approach to and builds the whole system incrementally in thin vertical slices across the
whole stack. Each sections maps to a corresponding branch. The scaffolding section ensures basic integration and unit
testing and "deploy"-ability. e next section focuses on the deployment tooling for onprem and cloud
deployments. Then sections 2-5 build the SCADA and embedded system incrementally. Sections 6-7 build the forecasting
API. Section 8 illustrates how a properly decoupled system can easily implement aws services (AWS IoT, Aurora, etc.). We
also do a cost analysis of using managed AWS services.

## Sections/Branches

### 00-system-setup
- integration and unit testing
- build artifacts
- versioning
- documentation
- seeding data

### 01-deployment-api
- onprem and cloud deployments
- scheduled, manual, and automatic deployments

### 02-boolean-reading
- modbus discrete input
- dnp3 binary input
- embedded systems digital input

### 03-float-reading
- modbus input register
- dnp3 analog input
- embedded analog input

### 04-boolean-command
- modbus force coil
- dnp3 binary output
- embedded systems digital output

### 05-float-command
- modbus holding register
- dnp3 analog output
- embedded systems analog output

### 06-demand-forecast
- ARIMA modeling
- rendering forecast

### 07-supply-forecast
- Prophet vs XGBoost vs LSTM evaluation
- rendering forecast
- dynamically retraining model

### 08-aws-services `BONUS`
- replace emqx with aws iot, timescale with timeseries, postgres with documentdb, nginx with s3, and sagemaker with the forecast-api
- creating new implementation based on abstract class
- cost analysis

# Project Status

| Branch               | 00-system-setup                                                                                              | 01-deployment-api                                                                                            | 02-boolean-reading                                                                                              | 03-float-reading                                                                                              | 04-boolean-command                                                                                              | 05-float-command                                                                                              | 06-demand-forecast                                                                                             | 07-supply-forecast                                                                                             | 08-aws-services                                                                                            |
|----------------------|--------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------|
| deployment-tooling   | ![](https://img.shields.io/gitlab/pipeline-status/fullstack-iot/deployment-tooling?branch=00-system-setup)   | ![](https://img.shields.io/gitlab/pipeline-status/fullstack-iot/deployment-tooling?branch=01-deployment-api) |                                                                                                                 |                                                                                                               |                                                                                                                 |                                                                                                               |                                                                                                                |                                                                                                                |                                                                                                            |
| power-controller     | ![](https://img.shields.io/gitlab/pipeline-status/fullstack-iot/power-controller?branch=00-system-setup)     |                                                                                                              | ![](https://img.shields.io/gitlab/pipeline-status/fullstack-iot/power-controller?branch=02-boolean-reading)     | ![](https://img.shields.io/gitlab/pipeline-status/fullstack-iot/power-controller?branch=03-float-reading)     | ![](https://img.shields.io/gitlab/pipeline-status/fullstack-iot/power-controller?branch=04-boolean-command)     | ![](https://img.shields.io/gitlab/pipeline-status/fullstack-iot/power-controller?branch=05-float-command)     |                                                                                                                |                                                                                                                | ![](https://img.shields.io/gitlab/pipeline-status/fullstack-iot/power-controller?branch=08-aws-services)   |                
| mock-modbus-server   | ![](https://img.shields.io/gitlab/pipeline-status/fullstack-iot/mock-modbus-server?branch=00-system-setup)   |                                                                                                              | ![](https://img.shields.io/gitlab/pipeline-status/fullstack-iot/mock-modbus-server?branch=02-boolean-reading)   | ![](https://img.shields.io/gitlab/pipeline-status/fullstack-iot/mock-modbus-server?branch=03-float-reading)   | ![](https://img.shields.io/gitlab/pipeline-status/fullstack-iot/mock-modbus-server?branch=04-boolean-command)   | ![](https://img.shields.io/gitlab/pipeline-status/fullstack-iot/mock-modbus-server?branch=05-float-command)   |                                                                                                                |                                                                                                                |                                                                                                            |
| mock-dnp3-outstation | ![](https://img.shields.io/gitlab/pipeline-status/fullstack-iot/mock-dnp3-outstation?branch=00-system-setup) |                                                                                                              | ![](https://img.shields.io/gitlab/pipeline-status/fullstack-iot/mock-dnp3-outstation?branch=02-boolean-reading) | ![](https://img.shields.io/gitlab/pipeline-status/fullstack-iot/mock-dnp3-outstation?branch=03-float-reading) | ![](https://img.shields.io/gitlab/pipeline-status/fullstack-iot/mock-dnp3-outstation?branch=04-boolean-command) | ![](https://img.shields.io/gitlab/pipeline-status/fullstack-iot/mock-dnp3-outstation?branch=05-float-command) |                                                                                                                |                                                                                                                |                                                                                                            |
| domain-api           | ![](https://img.shields.io/gitlab/pipeline-status/fullstack-iot/domain-api?branch=00-system-setup)           |                                                                                                              | ![](https://img.shields.io/gitlab/pipeline-status/fullstack-iot/domain-api?branch=02-boolean-reading)           | ![](https://img.shields.io/gitlab/pipeline-status/fullstack-iot/domain-api?branch=03-float-reading)           | ![](https://img.shields.io/gitlab/pipeline-status/fullstack-iot/domain-api?branch=04-boolean-command)           | ![](https://img.shields.io/gitlab/pipeline-status/fullstack-iot/domain-api?branch=05-float-command)           |                                                                                                                |                                                                                                                | ![](https://img.shields.io/gitlab/pipeline-status/fullstack-iot/domain-api?branch=08-aws-services)         |             
| scada-mqtt-gateway   | ![](https://img.shields.io/gitlab/pipeline-status/fullstack-iot/scada-mqtt-gateway?branch=00-system-setup)   |                                                                                                              | ![](https://img.shields.io/gitlab/pipeline-status/fullstack-iot/scada-mqtt-gateway?branch=02-boolean-reading)   | ![](https://img.shields.io/gitlab/pipeline-status/fullstack-iot/scada-mqtt-gateway?branch=03-float-reading)   | ![](https://img.shields.io/gitlab/pipeline-status/fullstack-iot/scada-mqtt-gateway?branch=04-boolean-command)   | ![](https://img.shields.io/gitlab/pipeline-status/fullstack-iot/scada-mqtt-gateway?branch=05-float-command)   |                                                                                                                |                                                                                                                | ![](https://img.shields.io/gitlab/pipeline-status/fullstack-iot/scada-mqtt-gateway?branch=08-aws-services) |
| realtime-dashboard   | ![](https://img.shields.io/gitlab/pipeline-status/fullstack-iot/realtime-dashboard?branch=00-system-setup)   |                                                                                                              | ![](https://img.shields.io/gitlab/pipeline-status/fullstack-iot/realtime-dashboard?branch=02-boolean-reading)   | ![](https://img.shields.io/gitlab/pipeline-status/fullstack-iot/realtime-dashboard?branch=03-float-reading)   | ![](https://img.shields.io/gitlab/pipeline-status/fullstack-iot/realtime-dashboard?branch=04-boolean-command)   | ![](https://img.shields.io/gitlab/pipeline-status/fullstack-iot/realtime-dashboard?branch=05-float-command)   |                                                                                                                |                                                                                                                | ![](https://img.shields.io/gitlab/pipeline-status/fullstack-iot/realtime-dashboard?branch=08-aws-services) |                
| forecast-api         | ![](https://img.shields.io/gitlab/pipeline-status/fullstack-iot/forecast-api?branch=00-system-setup)         |                                                                                                              |                                                                                                                 |                                                                                                               |                                                                                                                 |                                                                                                               | ![](https://img.shields.io/gitlab/pipeline-status/fullstack-iot/forecast-api?branch=06-energy-demand-forecast) | ![](https://img.shields.io/gitlab/pipeline-status/fullstack-iot/forecast-api?branch=07-energy-supply-forecast) | ![](https://img.shields.io/gitlab/pipeline-status/fullstack-iot/forecast-api?branch=08-aws-services)       |


